# Mogwai Framework
  
Mogwai Framework is a WIP experimental WordPress framework for helping developers quickly create custom themes that are highly configurable, and follow all WordPress conventions and standards.
  
## The Problem
  
Many theme developers take quite a few shortcuts during the build process, that hurt the final product. They may look great, but under the hood, they engine parts are stuck together with duct tape. Mogwai wants to change that, by making it seamless to build features - the right way.

## License
  
Mogwai is licensed under the GNU GPL v2. It's free as in freedom.
